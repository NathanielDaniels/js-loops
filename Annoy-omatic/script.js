//var thereYet = prompt("Are We There Yet?!");
//
//while ((thereYet !== "yes") && (thereYet !== "yeah")) {
//    var thereYet = prompt("Are We There Yet?!");
//}
//
//alert("Yay, We Finally Made It!");

//-----------------
//Version 2 (v2)
//-----------------
// Even if a string is use, indexOf will find the word "yes". 
var thereYet = prompt("Are We There Yet?!");

while (thereYet.indexOf("yes") === -1) {
    var thereYet = prompt("Are We There Yet?!");
}

alert("Yay, we made it!");


//-----------------


// Tried to build with "if" statements. Although it almost accomplished the same thing, it only runs a couple times tops. Where as the "while" loop, you can set it to repeat if what its looking for is not met.


//var thereYet = prompt("Are We There Yet?!");
//var answer = "["Yes", "yes", "yeah", "Yeah"];"

//if (thereYet == answer[0]) {
//    alert("Yay, we finally made it!");
//} else if (thereYet == answer[1]) {
//    alert("Yay, we finally made it!");
//} else if (thereYet == answer[2]) {
//    alert("Yay, we finally made it!");
//} else if (thereYet == answer[3]) {
//    alert("Yay, we finally made it!");
//} else {
//    var thereYet = prompt("Are We There Yet?!");
//}