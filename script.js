//var str = "hello";
//var count = 0;
//
//while(count < str.length) {
//    console.log(str[count]);
//    count++;
//}
//

//--------- While Loop Problem Set -------------------

console.log("print numbers between -10 to 19")
var x = -10;
while (x <= 19) {
    console.log(x);
    x++;
}

console.log("print even numbers between 10 to 40")
var x = 10;
while (x <= 40) {
    if (x % 2 == 0)
    console.log(x);
    x++; //or x+=2 (but remove if statement);
}

console.log("print Odd numbers between 300 to 333")
var x = 300;
while (x <= 333) {
    if (x % 2 !== 0) 
        console.log(x);
        x++;
}

console.log("print numbers divisible by 5 and 3, betwen 5 and 50")
var x = 5;
while (x <= 50) {
    if ((x % 5 === 0) && (x % 3 === 0)) 
        console.log(x);
        x++;
}